import { fetcher } from "@/lib/fetcher";
import {
  CircularProgress,
  FormControl,
  FormControlLabel,
  FormLabel,
  Radio,
  RadioGroup,
} from "@mui/material";
import { MetaTable } from "@prisma/client";
import { SetStateAction, useCallback, useEffect, useMemo } from "react";
import useSWR from "swr";

type FactTablesProps = {
  setFactTable: React.Dispatch<SetStateAction<MetaTable | undefined>>;
  setApiError: React.Dispatch<SetStateAction<boolean>>;
};

export const FactTables: React.FC<FactTablesProps> = ({
  setFactTable,
  setApiError,
}) => {
  const { data, error, isLoading } = useSWR<MetaTable[]>(
    "/api/getFactTables",
    fetcher
  );

  useEffect(() => {
    setApiError(!!error);
  }, [error, setApiError]);

  const handleChange = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      setFactTable(
        data?.find((table) => table.idTable === Number(event.target.value))
      );
    },
    [data, setFactTable]
  );

  return (
    <>
      {useMemo(() => {
        if (isLoading) {
          return (
            <div className="flex items-center justify-center h-full">
              <CircularProgress />
            </div>
          );
        }

        if (data) {
          return (
            <FormControl>
              <FormLabel id="demo-controlled-radio-buttons-group">
                Fact tables
              </FormLabel>
              <RadioGroup
                name="controlled-radio-buttons-group"
                onChange={handleChange}
              >
                {data?.map((table) => (
                  <FormControlLabel
                    style={{ userSelect: "none" }}
                    key={table.idTable}
                    value={table.idTable}
                    control={<Radio />}
                    label={table.tableName}
                  />
                ))}
              </RadioGroup>
            </FormControl>
          );
        }
      }, [data, handleChange, isLoading])}
    </>
  );
};
