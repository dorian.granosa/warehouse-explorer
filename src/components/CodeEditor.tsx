import "@uiw/react-textarea-code-editor/dist.css";
import dynamic from "next/dynamic";
import { SetStateAction } from "react";

import PlayArrowIcon from "@mui/icons-material/PlayArrow";
import StopIcon from "@mui/icons-material/Stop";
import { CircularProgress, IconButton } from "@mui/material";
import Stopwatch, { StopwatchRef } from "./Stopwatch";

const CodeEditorArea = dynamic(
  () => import("@uiw/react-textarea-code-editor").then((mod) => mod.default),
  { ssr: false }
);

type CodeEditorProps = {
  code: string;
  setCode: React.Dispatch<SetStateAction<string>>;
  query: () => Promise<void>;
  isQuerying: boolean;
  queryError: string | undefined;
  cancelQuery: () => void;
  stopwatchRef: React.RefObject<StopwatchRef>;
};

export const CodeEditor: React.FC<CodeEditorProps> = ({
  code,
  setCode,
  query,
  isQuerying,
  queryError,
  cancelQuery,
  stopwatchRef,
}) => {
  return (
    <div className="flex flex-col w-full h-full">
      <CodeEditorArea
        value={code}
        language="sql"
        onChange={(evn) => setCode(evn.target.value)}
        padding={15}
        className="grow"
        style={{
          overflowY: "auto",
          fontSize: 15,
          fontFamily:
            "ui-monospace,SFMono-Regular,SF Mono,Consolas,Liberation Mono,Menlo,monospace",
        }}
      />
      <div className="flex items-center gap-1">
        {!isQuerying && (
          <IconButton onClick={query} color="success">
            <PlayArrowIcon />
          </IconButton>
        )}
        {isQuerying && (
          <IconButton onClick={cancelQuery} color="error">
            <StopIcon />
          </IconButton>
        )}
        {isQuerying && <CircularProgress size={20} />}
        {queryError && <p className="text-red-500">Error: {queryError}</p>}
        <div className="grow" />
        <Stopwatch ref={stopwatchRef} />
      </div>
    </div>
  );
};
