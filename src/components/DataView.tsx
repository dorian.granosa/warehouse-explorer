import { DataGrid, GridColDef } from "@mui/x-data-grid";
import { useEffect, useState } from "react";

type DataViewProps = {
  data: object[] | undefined;
};

export const DataView: React.FC<DataViewProps> = ({ data }) => {
  const [columns, setColumns] = useState<GridColDef[]>([]);
  const [rows, setRows] = useState<object[]>([]);

  useEffect(() => {
    if (!data) return;

    if (data.length === 0) {
      setColumns([]);
      setRows([]);
      return;
    }

    setColumns(
      Object.keys(data[0]).map((key) => {
        return { field: key, headerName: key, width: 300 };
      })
    );

    setRows(
      data.map((row, i) => {
        return { id: i, ...row };
      })
    );
  }, [data]);

  return (
    <DataGrid
      className="w-full max-w-full"
      columns={columns}
      rows={rows}
      autoPageSize
    />
  );
};
