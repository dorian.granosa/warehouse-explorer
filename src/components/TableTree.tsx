import { fetcher } from "@/lib/fetcher";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Checkbox,
  CircularProgress,
  FormControlLabel,
} from "@mui/material";
import { MetaTable } from "@prisma/client";
import { SetStateAction, useEffect, useMemo } from "react";
import useSWR from "swr";

type TableTreeProps = {
  factTable?: MetaTable;
  setApiError: React.Dispatch<SetStateAction<boolean>>;
  setMeasures: React.Dispatch<SetStateAction<object[]>>;
  setDimensions: React.Dispatch<SetStateAction<object[]>>;
};

export const TableTree: React.FC<TableTreeProps> = ({
  factTable,
  setApiError,
  setMeasures,
  setDimensions,
}) => {
  const {
    data: measuresData,
    error: measuresError,
    isLoading: measuresLoading,
  } = useSWR(
    factTable && `/api/getFactMeasures?id=${factTable?.idTable}`,
    fetcher
  );

  const {
    data: dimensionsData,
    error: dimensionsError,
    isLoading: dimensionsLoading,
  } = useSWR(
    factTable && `/api/getFactDimensions?id=${factTable?.idTable}`,
    fetcher
  );

  useEffect(() => {
    setApiError(measuresError || dimensionsError);
  }, [measuresError, dimensionsError, setApiError]);

  return (
    <div className="w-full h-full">
      {useMemo(() => {
        if (measuresLoading || dimensionsLoading)
          return (
            <div className="flex items-center justify-center h-full">
              <CircularProgress />
            </div>
          );

        return (
          <>
            <Accordion>
              <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                Measures
              </AccordionSummary>
              <AccordionDetails>
                <div className="flex flex-col">
                  {measuresData?.map((measure: any) => (
                    <FormControlLabel
                      style={{ userSelect: "none" }}
                      key={measure.attrName}
                      control={<Checkbox />}
                      label={measure.attrName}
                      onChange={(event) => {
                        if (!(event.target as HTMLInputElement).checked) {
                          setMeasures((prev) =>
                            prev.filter(
                              (m) =>
                                JSON.stringify(m) !== JSON.stringify(measure)
                            )
                          );
                        } else {
                          setMeasures((prev) => [...prev, measure]);
                        }
                      }}
                    />
                  ))}
                </div>
              </AccordionDetails>
            </Accordion>
            <Accordion defaultExpanded={true}>
              <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                Dimensions
              </AccordionSummary>
              <AccordionDetails>
                {dimensionsData && (
                  <div className="flex flex-col">
                    {Object.keys(dimensionsData).map((tableName) => (
                      <Accordion key={tableName}>
                        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                          {tableName}
                        </AccordionSummary>
                        <AccordionDetails>
                          <div className="flex flex-col">
                            {dimensionsData[tableName].map((dimension: any) => (
                              <FormControlLabel
                                style={{ userSelect: "none" }}
                                key={`${dimension.nameSqlFactTable}.${dimension.factTabName}.${dimension.attrName}`}
                                control={<Checkbox />}
                                label={dimension.attrName}
                                onChange={(event) => {
                                  if (
                                    !(event.target as HTMLInputElement).checked
                                  ) {
                                    setDimensions((prev) =>
                                      prev.filter(
                                        (d) =>
                                          JSON.stringify(d) !==
                                          JSON.stringify(dimension)
                                      )
                                    );
                                  } else {
                                    setDimensions((prev) => [
                                      ...prev,
                                      dimension,
                                    ]);
                                  }
                                }}
                              />
                            ))}
                          </div>
                        </AccordionDetails>
                      </Accordion>
                    ))}
                  </div>
                )}
              </AccordionDetails>
            </Accordion>
          </>
        );
      }, [
        dimensionsData,
        dimensionsLoading,
        measuresData,
        measuresLoading,
        setDimensions,
        setMeasures,
      ])}
    </div>
  );
};
