import React, {
  ForwardRefRenderFunction,
  useEffect,
  useImperativeHandle,
  useRef,
  useState,
} from "react";

export interface StopwatchRef {
  start: () => void;
  stop: () => void;
}

interface StopwatchProps {}

const Stopwatch: ForwardRefRenderFunction<StopwatchRef, StopwatchProps> = (
  {},
  ref
) => {
  const intervalIdRef = useRef<number | null>(null);
  const startTimeRef = useRef<number | null>(null);
  const endTimeRef = useRef<number | null>(null);
  const [elapsedTime, setElapsedTime] = useState<number | null>(null);
  const [isRunning, setIsRunning] = useState(false);

  useImperativeHandle(ref, () => ({
    start: () => {
      if (!isRunning) {
        setIsRunning(true);
        startTimeRef.current = Date.now();
        intervalIdRef.current = window.setInterval(() => {
          endTimeRef.current = Date.now();
        }, 10);
      }
    },
    stop: () => {
      if (isRunning) {
        setIsRunning(false);
        window.clearInterval(intervalIdRef.current!);
        intervalIdRef.current = null;
        endTimeRef.current = Date.now();
      }
    },
  }));

  useEffect(() => {
    let requestId: number | null = null;

    const updateElapsedTime = () => {
      if (startTimeRef.current && endTimeRef.current) {
        const elapsedTime = endTimeRef.current - startTimeRef.current;
        setElapsedTime(elapsedTime);
      }
      requestId = requestAnimationFrame(updateElapsedTime);
    };

    if (isRunning) {
      requestId = requestAnimationFrame(updateElapsedTime);
    }

    return () => {
      if (requestId) {
        cancelAnimationFrame(requestId);
      }
    };
  }, [isRunning]);

  const formatTime = (time: number): string => {
    if (time < 0) {
      return "0.000";
    }

    const seconds = Math.floor(time / 1000);
    const milliseconds = time % 1000;
    return `${seconds}.${milliseconds.toString().padStart(3, "0")}`;
  };

  return (
    <div className="mr-2">
      {elapsedTime !== null ? formatTime(elapsedTime) : "0.000"}
    </div>
  );
};

export default React.forwardRef<StopwatchRef, StopwatchProps>(Stopwatch);
