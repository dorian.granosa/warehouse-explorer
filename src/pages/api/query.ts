import prisma from "@/lib/prisma";
import type { NextApiRequest, NextApiResponse } from "next";

export default async function handle(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const { code } = JSON.parse(req.body);

  try {
    const data = await prisma.$queryRawUnsafe(code);
    return res.json(data);
  } catch (error: any) {
    if (error.constructor.name === "PrismaClientKnownRequestError") {
      return res.status(500).json({ error: error.meta.message });
    }
  }

  return res.json([]);
}
