import prisma from "@/lib/prisma";
import type { NextApiRequest, NextApiResponse } from "next";

export default async function handle(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const { id } = req.query;

  const data = (await prisma.$queryRaw`SELECT *
  FROM MetaTableAttribute, MetaAgrFun, MetaTable, MetaTableAttributeAgrFun
  WHERE MetaTableAttribute.idTable = ${id}
  AND MetaTableAttribute.idTable = MetaTable.idTable
  AND MetaTableAttribute.idTable = MetaTableAttributeAgrFun.idTable
  AND MetaTableAttribute.attrOrdinal = MetaTableAttributeAgrFun.attrOrdinal
  AND MetaTableAttributeAgrFun.idAgrFun = MetaAgrFun.idAgrFun
  AND MetaTableAttribute.idTable = MetaTable.idTable
  AND idAttrType = 1
  ORDER BY MetaTableAttribute.attrOrdinal`) as any;

  return res.json(data);
}
