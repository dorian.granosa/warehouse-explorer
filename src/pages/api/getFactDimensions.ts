import prisma from "@/lib/prisma";
import type { NextApiRequest, NextApiResponse } from "next";

export default async function handle(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const { id } = req.query;

  const data = (await prisma.$queryRaw`SELECT dimTable.tableName
  , dimTable.tableSQLName AS nameSqlDimTable
  , factTable.tableSQLName AS nameSqlFactTable
  , factTabAtribut.attrSQLName AS factTabName
  , dimTabAtribut.attrSQLName AS dimTabName
  , MetaTableAttribute.*
  FROM MetaTableAttribute, MetaDimFact
  , MetaTable dimTable, MetaTable factTable
  , MetaTableAttribute factTabAtribut, MetaTableAttribute dimTabAtribut
  WHERE MetaDimFact.idDimTable = dimTable.idTable
  AND MetaDimFact.idFactTable = factTable.idTable
  AND MetaDimFact.idFactTable = factTabAtribut.idTable
  AND MetaDimFact.factAttrOrdinal = factTabAtribut.attrOrdinal
  AND MetaDimFact.idDimTable = dimTabAtribut.idTable
  AND MetaDimFact.dimAttrOrdinal = dimTabAtribut.attrOrdinal
  AND MetaTableAttribute.idTable = MetaDimFact.idDimTable
  AND idFactTable = ${id}
  AND MetaTableAttribute.idAttrType = 2
  ORDER BY dimTable.tableName, attrOrdinal`) as any;

  const dimensions: {
    [key: string]: any[];
  } = {};

  data.forEach((row: any) => {
    const tableName: string = row.tableName;
    if (!dimensions[tableName]) {
      dimensions[tableName] = [];
    }

    dimensions[tableName].push(row);
  });

  return res.json(dimensions);
}
