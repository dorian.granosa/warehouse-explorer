import prisma from "@/lib/prisma";
import type { NextApiRequest, NextApiResponse } from "next";

export default async function handle(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const tables = await prisma.metaTable.findMany({
    where: {
      idTableType: 1,
    },
  });

  return res.json(tables);
}
