import {
  CodeEditor,
  DataView,
  FactTables,
  Separator,
  StopwatchRef,
  TableTree,
} from "@/components";
import { cn } from "@/lib/cn";
import { fetcher } from "@/lib/fetcher";
import { queryGenerator } from "@/lib/queryGenerator";
import { Alert, Snackbar } from "@mui/material";
import { MetaTable } from "@prisma/client";
import { Inter } from "next/font/google";
import { useCallback, useEffect, useRef, useState } from "react";
import { useResizable } from "react-resizable-layout";

const inter = Inter({ subsets: ["latin"] });
let controller = new AbortController();

export default function Home() {
  const stopwatchRef = useRef<StopwatchRef>(null);
  const [code, setCode] = useState("");
  const [data, setData] = useState<object[]>();
  const [apiError, setApiError] = useState(false);
  const [isQuerying, setIsQuerying] = useState(false);
  const [queryError, setQueryError] = useState<string>();
  const [factTable, setFactTable] = useState<MetaTable>();
  const [measures, setMeasures] = useState<object[]>([]);
  const [dimensions, setDimensions] = useState<object[]>([]);

  useEffect(() => {
    setMeasures([]);
    setDimensions([]);
  }, [factTable]);

  useEffect(() => {
    if (!factTable) return;

    setCode(queryGenerator(factTable, measures, dimensions));
  }, [factTable, measures, dimensions]);

  const query = useCallback(async () => {
    setQueryError(undefined);
    setIsQuerying(true);
    stopwatchRef.current?.start();

    try {
      controller = new AbortController();
      const resp = await fetcher("/api/query", {
        method: "POST",
        body: JSON.stringify({
          code,
        }),
        signal: controller.signal,
      });

      setIsQuerying(false);
      stopwatchRef.current?.stop();

      if (resp.error) {
        setQueryError(resp.error);
        return;
      }

      if (resp) setData(resp);
    } catch (err: any) {
      setIsQuerying(false);
      stopwatchRef.current?.stop();

      if (err.name === "AbortError") return;

      setApiError(true);
    }
  }, [code]);

  const {
    isDragging: isMainDragging,
    position: mainW,
    separatorProps: mainSepBarProps,
  } = useResizable({
    axis: "x",
    initial: 420,
    min: 200,
  });

  const {
    isDragging: isLeftDragging,
    position: LeftH,
    separatorProps: leftSepBarProps,
  } = useResizable({
    axis: "y",
    initial: 450,
    min: 100,
    reverse: true,
  });

  const {
    isDragging: isRightDragging,
    position: rightH,
    separatorProps: rightSepBarProps,
  } = useResizable({
    axis: "y",
    initial: 600,
    min: 100,
    reverse: true,
  });

  return (
    <>
      <main className={`flex flex-column h-screen w-screen ${inter.className}`}>
        <div
          className={cn("shrink-0 ccontents", isMainDragging && "dragging")}
          style={{ width: mainW }}
        >
          <div className="flex flex-col h-full">
            <div className="p-3 overflow-auto grow">
              <FactTables
                setFactTable={setFactTable}
                setApiError={setApiError}
              />
            </div>
            <Separator
              dir={"horizontal"}
              isDragging={isLeftDragging}
              {...leftSepBarProps}
            />
            <div
              className={cn(
                "shrink-0 overflow-y-auto",
                isLeftDragging && "dragging"
              )}
              style={{ height: LeftH }}
            >
              <TableTree
                factTable={factTable}
                setApiError={setApiError}
                setMeasures={setMeasures}
                setDimensions={setDimensions}
              />
            </div>
          </div>
        </div>
        <Separator isDragging={isMainDragging} {...mainSepBarProps} />
        <div
          className="flex flex-col h-full grow"
          style={{
            width: `calc(100vw - ${mainW}px - 5px)`,
          }}
        >
          <div
            className="overflow-hidden grow"
            style={{ height: `calc(100vh - ${rightH}px)` }}
          >
            <CodeEditor
              code={code}
              setCode={setCode}
              query={query}
              isQuerying={isQuerying}
              queryError={queryError}
              cancelQuery={() => controller.abort()}
              stopwatchRef={stopwatchRef}
            />
          </div>
          <Separator
            dir={"horizontal"}
            isDragging={isRightDragging}
            {...rightSepBarProps}
          />
          <div
            className={cn("shrink-0", isRightDragging && "dragging")}
            style={{ height: rightH }}
          >
            <DataView data={data} />
          </div>
        </div>
      </main>

      <Snackbar
        open={apiError}
        autoHideDuration={6000}
        onClose={() => setApiError(false)}
      >
        <Alert severity="error">
          There was an error connecting to the API. Please check your connection
        </Alert>
      </Snackbar>
    </>
  );
}
