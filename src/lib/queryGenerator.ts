import { MetaTable } from "@prisma/client";

const factTableGenerator = (factTable: MetaTable) => {
  return `SELECT *
FROM ${factTable.tableSQLName.trim()}`;
};

const measuresDimensionsGenerator = (
  factTable: MetaTable,
  measures: object[],
  dimensions: object[]
) => {
  const distinctDimensions: object[] = [];
  dimensions.forEach((dimension: any) => {
    if (
      !distinctDimensions.find(
        (distinctDimension: any) =>
          JSON.stringify(distinctDimension) === JSON.stringify(dimension)
      )
    ) {
      distinctDimensions.push({
        nameSqlDimTable: dimension.nameSqlDimTable,
        nameSqlFactTable: dimension.nameSqlFactTable,
        factTabName: dimension.factTabName,
        dimTabName: dimension.dimTabName,
      });
    }
  });

  const select: string[] = [];
  measures.forEach((measure: any) => {
    select.push(
      `${measure.agrFunName.trim()}(${measure.tableSQLName.trim()}.${measure.attrSQLName.trim()}) AS '${measure.attrName.trim()}'`
    );
  });
  dimensions.forEach((dimension: any) => {
    select.push(
      `${dimension.nameSqlDimTable.trim()}.${dimension.attrSQLName.trim()} AS '${dimension.attrName.trim()}'`
    );
  });

  const from: string[] = [];
  from.push(factTable.tableSQLName.trim());
  distinctDimensions.forEach((dimension: any) => {
    if (!from.includes(dimension.nameSqlDimTable.trim()))
      from.push(dimension.nameSqlDimTable.trim());
  });

  const where: string[] = [];
  distinctDimensions.forEach((dimension: any) => {
    where.push(
      `${dimension.nameSqlFactTable.trim()}.${dimension.factTabName.trim()} = ${dimension.nameSqlDimTable.trim()}.${dimension.dimTabName.trim()}`
    );
  });

  const groupBy: string[] = [];
  dimensions.forEach((dimension: any) => {
    groupBy.push(
      `${dimension.nameSqlDimTable.trim()}.${dimension.attrSQLName.trim()}`
    );
  });

  if (where.length === 0 && groupBy.length === 0)
    return `SELECT ${select.join(", ")}
FROM ${from.join(", ")}`;

  return `SELECT ${select.join(", ")}
FROM ${from.join(", ")}
WHERE ${where.join(" AND ")}
GROUP BY ${groupBy.join(", ")}`;
};

export const queryGenerator = (
  factTable: MetaTable,
  measures: object[],
  dimensions: object[]
) => {
  if (measures.length === 0 && dimensions.length === 0)
    return factTableGenerator(factTable);

  return measuresDimensionsGenerator(factTable, measures, dimensions);
};
